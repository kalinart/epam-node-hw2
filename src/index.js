require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 8080;
const DB_URL =
  'mongodb+srv://admin:qwerty67@notescluster0.0pzgg.mongodb.net/notes?retryWrites=true&w=majority';
const notesRouter = require('./routes/noteRouter');
const authRouter = require('./routes/authRouter');
const meRouter = require('./routes/meRouter');
const {authMiddleware} = require('./middlewares/authMiddleware');

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

// app.use(authMiddleware);
// app.use('/api/notes', notesRouter);
// app.use('/api/users/me', meRouter);

app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users/me', [authMiddleware], meRouter);

// 404
app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});

// parameters validation
app.use((err, req, res, next) => {
  // custom error
  res.status(500).json({message: err.message});
});

// app.get('*', (req, res) => {
//   res.status(404).send('Not found');
// });

const start = async () => {
  try {
    await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    console.log('Connected to DB');

    app.listen(PORT, () => {
      console.log('Server has been started on port ', PORT);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
