const express = require('express');
const router = express.Router();
const {
  getNotes,
  createNote,
  getNote,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
} = require('../controllers/notesControllers');

// /api/notes
router.route('/').get(getNotes).post(createNote);
router
    .route('/:id')
    .get(getNote)
    .put(updateUserNoteById)
    .patch(toggleCompletedForUserNoteById)
    .delete(deleteUserNoteById);

module.exports = router;
