const express = require('express');
const router = express.Router();
const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require('../controllers/meControllers');

// /api/users/me
router
    .route('/')
    .get(getProfileInfo)
    .delete(deleteProfile)
    .patch(changeProfilePassword);

module.exports = router;
