const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, offset, limit) => {
  offset = parseInt(offset) || 0;
  limit = parseInt(limit) || 5;

  const count = await Note.find({userId}).countDocuments();
  const notes = await Note.find({userId}).skip(offset).limit(limit);

  return {offset, limit, count, notes};
};

module.exports = {
  getNotesByUserId,
};
