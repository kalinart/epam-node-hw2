const {registration, signIn} = require('../services/authService');

const signup = async (req, res) => {
  const {username, password} = req.body;

  try {
    await registration({username, password});

    res.json({message: 'Account created successfully!'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

const signin = async (req, res) => {
  const {username, password} = req.body;

  try {
    const jwt_token = await signIn({username, password});

    res.json({jwt_token, message: 'Logged in successfully!'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports = {
  signup,
  signin,
};
