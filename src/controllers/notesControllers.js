const {Note} = require('../models/noteModel');
const {getNotesByUserId} = require('../services/notesServices');

// GET /api/notes
const getNotes = async (req, res) => {
  const {offset, limit} = req.query;
  const {userId} = req.user;

  try {
    const notes = await getNotesByUserId(userId, offset, limit);
    res.json(notes);
  } catch (error) {
    console.log(error);
    res.status(500).json({message: error});
  }
};

// POST /api/notes
const createNote = async (req, res) => {
  const {text} = req.body;
  const {userId} = req.user;

  if (!text) {
    return res.status(400).json({message: `Please specify 'text' parameter`});
  }

  try {
    const note = new Note({text, userId});
    await note.save();

    res.json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};

// GET /api/notes/:id
const getNote = async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  if (!id) {
    return res.status(400).json({
      message: 'string',
    });
  }

  try {
    const note = await Note.findOne({_id: id, userId});
    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }
    res.json({note});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};

// PUT /api/notes/:id
const updateUserNoteById = async (req, res) => {
  const {text} = req.body;
  // const text = req.body;
  const {userId} = req.user;
  const {id} = req.params;

  if (!text) {
    return res.status(400).json({message: `Please specify 'text' parameter`});
  }

  try {
    // findOneAndUpdate({_id: id, userId}, {$set: text})
    await Note.updateOne({_id: id, userId}, {text});
    res.json({message: 'Success'});
  } catch (error) {
    console.log(error);
    res.status(500).json({message: 'Server error'});
  }
};

// PUTCH /api/notes/:id
const toggleCompletedForUserNoteById = async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  try {
    const note = await Note.findOne({_id: id, userId});
    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }

    const completed = note.completed ? false : true;

    await Note.updateOne({_id: id, userId}, {completed});
    res.json({message: 'Success'});
  } catch (error) {
    console.log(error);
    res.status(500).json({message: 'Server error'});
  }
};

// DELETE /api/notes/:id
const deleteUserNoteById = async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  try {
    const note = await Note.findOneAndDelete({_id: id, userId});

    if (!note) {
      return res.status(400).json({message: 'Note not found'});
    }

    res.json({message: 'Success'});
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
};

module.exports = {
  getNotes,
  createNote,
  getNote,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
};
